﻿<?php
$audio = array(
    "audio" => array(
        array(
            "title" => "มาร์ช ม.ข" ,
				"author" => array(
					"petition" => "พร พิรุณ",
					"melody" => "เอื้อ สุนทรสนาน"
					),
			"content" => "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา"
        ),
		
        array(
            "title" => "เสียงสนครวญ",
				"author" => array(
					"petition" => "สุนทราภรณ์",
					"melody" => "สุนทราภรณ์"
					),
			"content" => "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว"	
        )
    )
);
echo json_encode($audio);
?>